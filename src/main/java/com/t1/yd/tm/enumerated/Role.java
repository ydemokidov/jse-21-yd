package com.t1.yd.tm.enumerated;

public enum Role {

    ADMIN("Administrator"),
    USUAL("Usual User");

    private final String displayName;

    Role(final String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
