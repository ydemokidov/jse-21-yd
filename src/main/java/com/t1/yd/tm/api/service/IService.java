package com.t1.yd.tm.api.service;

import com.t1.yd.tm.api.repository.IRepository;
import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.model.AbstractEntity;

import java.util.List;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

    List<E> findAll(Sort sort);

}
