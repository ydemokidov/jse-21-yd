package com.t1.yd.tm.model;

public final class Command {

    private String argument;

    private String name;

    private String description;

    public Command() {
    }

    public Command(final String argument, final String name, final String description) {
        this.argument = argument;
        this.name = name;
        this.description = description;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(final String argument) {
        this.argument = argument;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return String.format("%s%s : %s", name, argument != null ? ", " + argument : "", description);
    }
}
