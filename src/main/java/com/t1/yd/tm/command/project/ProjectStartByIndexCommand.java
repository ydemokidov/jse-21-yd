package com.t1.yd.tm.command.project;

import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    public static final String NAME = "project_start_by_index";
    public static final String DESCRIPTION = "Start project by Index";

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        getProjectService().changeStatusByIndex(userId, index, Status.IN_PROGRESS);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
