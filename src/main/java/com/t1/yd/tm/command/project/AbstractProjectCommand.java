package com.t1.yd.tm.command.project;

import com.t1.yd.tm.api.service.IProjectService;
import com.t1.yd.tm.api.service.IProjectTaskService;
import com.t1.yd.tm.command.AbstractCommand;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected IProjectService getProjectService() {
        return serviceLocator.getProjectService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESC: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
    }

}
