package com.t1.yd.tm.command.user;

import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.model.User;

public class UserShowProfileCommand extends AbstractUserCommand {

    private final String name = "user_show_profile";

    private final String description = "Show user profile";

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        final User user = getAuthService().getUser();
        System.out.println("[USER PROFILE:]");
        showUser(user);
        System.out.println("Last Name: " + user.getLastName());
        System.out.println("First Name: " + user.getFirstName());
        System.out.println("Middle Name: " + user.getMiddleName());
        System.out.println("Role: " + user.getRole().getDisplayName());
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

}
