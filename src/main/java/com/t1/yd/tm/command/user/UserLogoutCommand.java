package com.t1.yd.tm.command.user;

import com.t1.yd.tm.enumerated.Role;

public class UserLogoutCommand extends AbstractUserCommand {

    private final String name = "user_logout";
    private final String description = "User logout";

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getAuthService().logout();
        System.out.println("[USER LOGGED OUT]");
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

}
