package com.t1.yd.tm.command.system;

public class ApplicationVersionCommand extends AbstractSystemCommand {

    public static final String NAME = "version";
    public static final String ARGUMENT = "-v";
    public static final String DESCRIPTION = "Show program version";

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}
