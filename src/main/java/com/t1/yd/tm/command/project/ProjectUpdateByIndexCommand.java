package com.t1.yd.tm.command.project;

import com.t1.yd.tm.model.Project;
import com.t1.yd.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    public static final String NAME = "project_update_by_index";
    public static final String DESCRIPTION = "Update project by Index";

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY INDEX]");

        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber();

        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();

        System.out.println("ENTER DESCRIPTION:");
        final String desc = TerminalUtil.nextLine();
        final String userId = getUserId();
        final Project project = getProjectService().updateByIndex(userId, index, name, desc);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
