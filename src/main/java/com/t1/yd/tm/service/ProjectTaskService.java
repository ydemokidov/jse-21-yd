package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.IProjectRepository;
import com.t1.yd.tm.api.repository.ITaskRepository;
import com.t1.yd.tm.api.service.IProjectTaskService;
import com.t1.yd.tm.exception.entity.ProjectNotFoundException;
import com.t1.yd.tm.exception.entity.TaskNotFoundException;
import com.t1.yd.tm.exception.field.ProjectIdEmptyException;
import com.t1.yd.tm.exception.field.TaskIdEmptyException;
import com.t1.yd.tm.exception.field.UserIdEmptyException;
import com.t1.yd.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectTaskService(final ITaskRepository taskRepository, final IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public void bindTaskToProject(final String userId, final String taskId, final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();

        final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
    }

    @Override
    public void removeProjectById(final String userId, final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
        tasks.forEach(e -> taskRepository.removeById(userId, e.getId()));
        projectRepository.removeById(userId, projectId);
    }

    @Override
    public void unbindTaskFromProject(final String userId, final String taskId, final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
    }
}
